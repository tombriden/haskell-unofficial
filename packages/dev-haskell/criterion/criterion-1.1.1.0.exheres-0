# Copyright 2012 Ivan Lazar Miljenovic <Ivan.Miljenovic@gmail.com>
# Distributed under the terms of the GNU General Public License v2

require hackage

SUMMARY="Robust, reliable performance measurement and analysis"
DESCRIPTION="
This library provides a powerful but simple way to measure software
performance. It provides both a framework for executing and analysing
benchmarks and a set of driver functions that makes it easy to build
and run benchmarks, and to analyse their results.

The fastest way to get started is to read the documentation and
examples in the Criterion.Main module.

For an example of the kinds of reports that criterion generates, see
http://bos.github.com/criterion/.
"
HOMEPAGE="https://github.com/bos/criterion ${HOMEPAGE}"

LICENCES="BSD-3"
PLATFORMS="~amd64"
MYOPTIONS=""

DEPENDENCIES="
    $(haskell_lib_dependencies "
        dev-haskell/Glob[>=0.7.2]
        dev-haskell/aeson[>=0.8]
        dev-haskell/ansi-wl-pprint[>=0.6.7.2]
        dev-haskell/binary[>=0.5.1.0]
        dev-haskell/bytestring[>=0.9&<1.0]
        dev-haskell/cassava[>=0.3.0.0]
        dev-haskell/containers
        dev-haskell/deepseq[>=1.1.0.0]
        dev-haskell/directory
        dev-haskell/filepath
        dev-haskell/hastache[>=0.6.0]
        dev-haskell/mtl[>=2]
        dev-haskell/mwc-random[>=0.8.0.3]
        dev-haskell/optparse-applicative[>=0.11]
        dev-haskell/parsec[>=3.1.0]
        dev-haskell/statistics[>=0.13.2.1]
        dev-haskell/text[>=0.11]
        dev-haskell/time
        dev-haskell/transformers
        dev-haskell/vector[>=0.7.1]
        dev-haskell/vector-algorithms[>=0.4]
    ")
"

RESTRICT="test"

