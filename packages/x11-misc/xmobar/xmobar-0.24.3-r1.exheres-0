# Copyright 2008 Santiago M. Mola
# Copyright 2009 Ingmar Vanhassel
# Copyright 2010, 2011 Markus Rothe
# Copyright 2013 Jorge Aparicio
# Distributed under the terms of the GNU General Public License v2

require hackage [ has_bin=true has_lib=false ]

SUMMARY="Minimalistic text based status bar"
DESCRIPTION="
Inspired by the Ion3 status bar, it supports similar features, like dynamic
color management, output templates, and extensibility through plugins.
"
HOMEPAGE="http://projects.haskell.org/xmobar/"

LICENCES="BSD-3"
SLOT="0"
PLATFORMS="~amd64"
MYOPTIONS="
    alsa [[
        description = [ Use alsa-mixer to get the volume from soundcards ]
    ]]
    conduit [[ description = [ Use http-conduits for getting weather data ] ]]
    mail [[ description = [ Build Mail and MBox plugins ] ]]
    mpd [[ description = [ Support the Music Player Daemon ] ]]
    mpris [[
        description = [
            Support the Media Player Remote Interfacing Specification
        ]
    ]]
    wireless [[ description = [ Enables display of wireless connection state and quality ] ]]
"

DEPENDENCIES="
    alsa? (
        $(haskell_bin_dependencies "
            dev-haskell/alsa-core[~>0.5.0]
            dev-haskell/alsa-mixer[>0.2.0.2]
        ")
    )
    conduit? (
        $(haskell_bin_dependencies "
            dev-haskell/http-conduit
            dev-haskell/http-types
        ")
    )
    mail? (
        $(haskell_bin_dependencies "
            dev-haskell/hinotify[~>0.3.0]
        ")
    )
    mpd? (
        $(haskell_bin_dependencies "
            dev-haskell/libmpd[~>0.9.0]
        ")
    )
    mpris? (
        $(haskell_bin_dependencies "
            dev-haskell/dbus[>=0.10]
        ")
    )
    wireless? (
        build+run:
            net-wireless/wireless_tools
    )
    $(haskell_bin_dependencies "
        dev-haskell/HTTP[>=4000.2.4]
        dev-haskell/X11-xft[>=0.2&<0.4]
        dev-haskell/X11[>=1.6.1]
        dev-haskell/bytestring
        dev-haskell/containers
        dev-haskell/directory
        dev-haskell/filepath
        dev-haskell/mtl[>=2.1&<2.3]
        dev-haskell/old-locale
        dev-haskell/parsec[~>3.1.0]
        dev-haskell/process
        dev-haskell/regex-compat
        dev-haskell/stm[>=2.3&<2.5]
        dev-haskell/time
        dev-haskell/transformers
        dev-haskell/unix
        dev-haskell/utf8-string[>=0.3.0&<1.1]
    ")
    build+run:
        x11-libs/libXpm
"

# TODO make datezone optional or enable unconditional

CABAL_SRC_CONFIGURE_PARAMS=(
    --flags=-with_datezone
    --flags=with_xft
    --flags=with_xpm
)

CABAL_SRC_CONFIGURE_OPTION_FLAGS=(
    'alsa with_alsa'
    'conduit with_conduit'
    'mpris with_dbus'
    'mail with_inotify'
    'mpd with_mpd'
    'mpris with_mpris'
    'wireless with_iwlib'
)
