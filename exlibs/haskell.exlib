# Copyright 2009, 2010 Ingmar Vanhassel
# Copyright 2009 David Leverton
# Distributed under the terms of the GNU General Public License v2

# alternatives for convenience, since Haskell packages (especially
# Cabal) tend to be easily SLOTtable, and it's a bit of a pain to have
# to call both haskell_ and alternatives_ pkg_* functions manually.
# This can go away if/when we have some kind of trigger system, so
# that exlib functions con't need to be called explicitly.
require alternatives

myexparam ghc_dep=

myexparam -b has_lib=true
myexparam -b has_bin=false

if exparam -b has_lib; then
    SLOT=${PV}

    myexparam -b has_profile=true
    exparam -b has_profile && MYOPTIONS+=" profile"
fi

# TODO: should remove the 6.10 hardcode.  Currently we need it for
# ghc-pkg --force, which would be simple to get round, and Cabal 1.6
# in cabal.exlib, which would be less simple (needs a good way to
# handle upgrades of bundled libraries)
haskell_ghc_dep() {
    local dep
    exparam -v dep ghc_dep
    echo ">=6.10.1${dep:+&${dep}}"
}

exparam -b has_lib && DEPENDENCIES+="
    build+run:
        dev-lang/ghc:=[$(haskell_ghc_dep)]
"

# XXX the :* shouldn't be necessary in build:, but Paludis is picky
exparam -b has_bin && DEPENDENCIES+="
    build:
        dev-lang/ghc:*[$(haskell_ghc_dep)]
    build+run:
        dev-libs/gmp:=
        dev-libs/libffi
"

haskell_bin_dependencies() {
    local oldset=${-}
    set -f
    echo \(

    echo build+run:

    local arg lib
    for arg in "${@}"; do
        if [[ ${arg} == \[\[* ]]; then
            echo "${arg}"
        else

            for lib in ${arg}; do
                local square=
                if [[ ${lib} == *\[* ]]; then
                    square=\[${lib#*\[}
                    lib=${lib%%\[*}
                fi
                [[ ${lib} == *:* ]] || lib+=:=
                echo ${lib}${square}
            done
        fi
    done

    echo \)
    [[ ${oldset} == *f* ]] || set +f
}

haskell_test_dependencies() {
    local oldset=${-}
    set -f
    echo \(

    echo test:

    local arg lib
    for arg in "${@}"; do
        if [[ ${arg} == \[\[* ]]; then
            echo "${arg}"
        else

            for lib in ${arg}; do
                local square=
                if [[ ${lib} == *\[* ]]; then
                    square=\[${lib#*\[}
                    lib=${lib%%\[*}
                fi
                [[ ${lib} == *:* ]] || lib+=':*'
                echo ${lib}${square}
            done
        fi
    done

    echo \)
    [[ ${oldset} == *f* ]] || set +f
}

haskell_lib_dependencies() {
    local oldset=${-}
    set -f
    echo \(

    echo build+run:

    local arg lib
    for arg in "${@}"; do
        if [[ ${arg} == \[\[* ]]; then
            echo "${arg}"
        else

            for lib in ${arg}; do
                local square=
                if [[ ${lib} == *\[* ]]; then
                    square=\[${lib#*\[}
                    lib=${lib%%\[*}
                fi
                [[ ${lib} == *:* ]] || lib+=:=
                exparam -b has_profile && square+='[profile?]'
                echo ${lib}${square}
            done
        fi
    done

    echo \)
    [[ ${oldset} == *f* ]] || set +f
}

haskell_is_bundled() {
    has ${PNV} $(ghc-pkg --global-package-db=/usr/$(exhost --target)/lib/ghc-$(haskell_ghc_version)/package.conf.d.shipped \
        --no-user-package-db --simple-output -v0 list ${PN})
}

haskell_ghc_version() {
    local ghc=$(best_version "dev-lang/ghc[$(haskell_ghc_dep)]")
    ghc=${ghc#dev-lang/ghc-}
    ghc=${ghc%::*}
    ghc=${ghc%:*}
    ghc=${ghc%-r*}
    echo ${ghc}
}

haskell_ghc_setup_paths() {
    eval "$(ROOT=/ eclectic ghc     script --sh $(haskell_ghc_version))"
    eval "$(ROOT=/ eclectic hsc2hs  script --sh $(basename /etc/env.d/alternatives/hsc2hs/*_ghc-$(haskell_ghc_version)))"
}

# runhaskell wrapper, executes /usr/bin/runhaskell with the given arguments
#
# This could support other haskell compilers besides ghc at some point, but
# currently we only have GHC packaged.
exrunhaskell() {
    echo "runhaskell ${@}"
    runhaskell "${@}" || die_unless_nonfatal "runhaskell ${@} failed"
}

haskell_doregistration() {
    # We --force registering and unregistering to work around GHC's dependency checks
    sed -e "s/'\(update\|unregister\)' /&'--force' /" -i register.sh unregister.sh || die "Appending --force to update/unregister command failed"

    HASKELL_REGISTRATION=/usr/$(exhost --target)/lib/ghc-$(haskell_ghc_version)/registration/${PNV}
    (
        insinto ${HASKELL_REGISTRATION}
        doins register.sh unregister.sh
    )
}

haskell_pkg_prerm() {
    # FIXME: ${ROOT}
    if [[ -n ${HASKELL_REGISTRATION} ]]; then
        echo "sh ${HASKELL_REGISTRATION}/unregister.sh"
        sh ${HASKELL_REGISTRATION}/unregister.sh ||
            ewarn "sh ${HASKELL_REGISTRATION}/unregister.sh: returned exit status: $?"
    fi

    alternatives_pkg_prerm
}

haskell_pkg_postinst() {
    # FIXME: ${ROOT}
    if [[ -n ${HASKELL_REGISTRATION} ]]; then
        echo "sh ${HASKELL_REGISTRATION}/register.sh"
        sh ${HASKELL_REGISTRATION}/register.sh ||
            ewarn "sh ${HASKELL_REGISTRATION}/register.sh: returned exit status: $?"
    fi

    alternatives_pkg_postinst
}

export_exlib_phases pkg_postinst pkg_prerm

